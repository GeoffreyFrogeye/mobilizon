defmodule Mobilizon.AddressesTest do
  use Mobilizon.DataCase

  alias Mobilizon.Addresses

  describe "addresses" do
    alias Mobilizon.Addresses.Address

    @valid_attrs %{
      country: "some addressCountry",
      locality: "some addressLocality",
      region: "some addressRegion",
      description: "some description",
      floor: "some floor",
      postal_code: "some postalCode",
      street: "some streetAddress",
      geom: %Geo.Point{coordinates: {10, -10}, srid: 4326}
    }
    @update_attrs %{
      country: "some updated addressCountry",
      locality: "some updated addressLocality",
      region: "some updated addressRegion",
      description: "some updated description",
      floor: "some updated floor",
      postal_code: "some updated postalCode",
      street: "some updated streetAddress",
      geom: %Geo.Point{coordinates: {20, -20}, srid: 4326}
    }
    # @invalid_attrs %{
    #   addressCountry: nil,
    #   addressLocality: nil,
    #   addressRegion: nil,
    #   description: nil,
    #   floor: nil,
    #   postalCode: nil,
    #   streetAddress: nil,
    #   geom: nil
    # }

    def address_fixture(attrs \\ %{}) do
      {:ok, address} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Addresses.create_address()

      address
    end

    test "list_addresses/0 returns all addresses" do
      address = address_fixture()
      assert [address.id] == Addresses.list_addresses() |> Enum.map(& &1.id)
    end

    test "get_address!/1 returns the address with given id" do
      address = address_fixture()
      assert Addresses.get_address!(address.id).id == address.id
    end

    test "create_address/1 with valid data creates a address" do
      assert {:ok, %Address{} = address} = Addresses.create_address(@valid_attrs)
      assert address.country == "some addressCountry"
      assert address.locality == "some addressLocality"
      assert address.region == "some addressRegion"
      assert address.description == "some description"
      assert address.floor == "some floor"
      assert address.postal_code == "some postalCode"
      assert address.street == "some streetAddress"
    end

    test "update_address/2 with valid data updates the address" do
      address = address_fixture()
      assert {:ok, %Address{} = address} = Addresses.update_address(address, @update_attrs)
      assert address.country == "some updated addressCountry"
      assert address.locality == "some updated addressLocality"
      assert address.region == "some updated addressRegion"
      assert address.description == "some updated description"
      assert address.floor == "some updated floor"
      assert address.postal_code == "some updated postalCode"
      assert address.street == "some updated streetAddress"
    end

    test "delete_address/1 deletes the address" do
      address = address_fixture()
      assert {:ok, %Address{}} = Addresses.delete_address(address)
      assert_raise Ecto.NoResultsError, fn -> Addresses.get_address!(address.id) end
    end

    test "change_address/1 returns a address changeset" do
      address = address_fixture()
      assert %Ecto.Changeset{} = Addresses.change_address(address)
    end

    test "process_geom/2 with valid data returns a Point element" do
      attrs = %{"type" => "point", "data" => %{"latitude" => 10, "longitude" => -10}}
      assert {:ok, %Geo.Point{}} = Addresses.process_geom(attrs)
    end

    test "process_geom/2 with invalid data returns nil" do
      attrs = %{"type" => :point, "data" => %{"latitude" => nil, "longitude" => nil}}
      assert {:error, "Latitude and longitude must be numbers"} = Addresses.process_geom(attrs)

      attrs = %{"type" => :not_valid, "data" => %{"latitude" => nil, "longitude" => nil}}
      assert {:error, :invalid_type} == Addresses.process_geom(attrs)
    end
  end
end
