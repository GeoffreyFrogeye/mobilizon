export interface ICurrentUser {
  id: number;
  email: string;
  isLoggedIn: boolean;
}
